[metadata]
name = rosbags
version = 0.9.0
author = Ternaris
author_email = team@ternaris.com
home_page = https://gitlab.com/ternaris/rosbags
description = Pure Python library to read, modify, convert, and write rosbag files.
long_description = file: README.rst
long_description_content_type = text/x-rst
keywords =
    cdr
    conversion
    deserialization
    idl
    message
    msg
    reader
    ros
    rosbag
    rosbag2
    serialization
    writer
license = Apache 2.0
license_files = LICENSE.txt
platform = any
classifiers =
    Development Status :: 4 - Beta
    License :: OSI Approved :: Apache Software License
    Programming Language :: Python
    Programming Language :: Python :: 3 :: Only
    Programming Language :: Python :: 3.8
    Programming Language :: Python :: 3.9
    Topic :: Scientific/Engineering
    Typing :: Typed
project_urls =
  Code = https://gitlab.com/ternaris/rosbags
  Documentation = https://ternaris.gitlab.io/rosbags
  Issue tracker = https://gitlab.com/ternaris/rosbags/issues

[options]
include_package_data = true
package_dir =
    = src
packages = find_namespace:
zip_safe = false
python_requires =
    >=3.8.2
setup_requires =
    setuptools >=40.8.0
    wheel
install_requires =
    lz4
    numpy
    ruamel.yaml
    zstandard

[options.entry_points]
console_scripts =
    rosbags-convert = rosbags.convert.__main__:main

[options.extras_require]
dev =
    darglint
    flake8
    flake8-annotations
    flake8-bugbear
    flake8-commas
    flake8-comprehensions
    flake8-docstrings
    flake8-fixme
    flake8-isort
    flake8-mutable
    flake8-print
    flake8-pytest-style
    flake8-quotes
    flake8-return
    flake8-simplify
    flake8-type-checking
    flake8-use-fstring
    pep8-naming
    pytest
    pytest-cov
    pytest-flake8
    pytest-mypy
    pytest-pylint
    sphinx
    sphinx-autodoc-typehints
    sphinx-rtd-theme

[options.packages.find]
where = src

[sdist]
formats = gztar, zip

[coverage:report]
exclude_lines =
    pragma: no cover
    if TYPE_CHECKING:
    if __name__ == '__main__':

[flake8]
avoid-escape = False
docstring_convention = google
docstring_style = google
extend-select = 
    # docstrings
    D204,
    D400,
    D401,
    D404,
    D413,
ignore =
    # do not require annotation of `self`
    ANN101,
    # allow line break before binary operator
    W503,
max-line-length = 100
strictness = long
suppress-none-returning = True

[isort]
include_trailing_comma = True
line_length = 100
multi_line_output = 3

[mypy]
ignore_missing_imports = True

[pydocstyle]
convention = google
add-select = D204,D400,D401,D404,D413

[pylint.FORMAT]
max-line-length = 100

[pylint.'MESSAGES CONTROL']
disable =
    duplicate-code,
    ungrouped-imports,

[yapf]
based_on_style = google
column_limit = 100
allow_split_before_dict_value = false
dedent_closing_brackets = true
indent_dictionary_value = false

[tool:pytest]
addopts =
  -v
  --flake8
  --mypy
  --pylint
  --cov=src
  --cov-branch
  --cov-report=html
  --cov-report=term
  --no-cov-on-fail
  --junitxml=report.xml
junit_family=xunit2
